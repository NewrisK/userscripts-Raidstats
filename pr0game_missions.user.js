// ==UserScript==
// @name         pr0game mission align
// @namespace    https://pr0game.pebkac.me/
// @version      0.1
// @description  Display missions align and in overview as one line. Not made use on for small displays!
// @author       DawnOfTheUwe
// @match        https://pr0game.com/*/game.php
// @match        https://pr0game.com/*/game.php?*page=overview*
// @match        https://pr0game.com/*/game.php?*page=phalanx*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_missions.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_missions.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    //overview
    GM_addStyle('content { max-height: fit-content; max-width: fit-content; }');
    GM_addStyle('ul, ol { padding: 0 0 0 1px; text-align: start; }');

    //phalanx
    GM_addStyle('td, .tip { text-align: start; }');

})();
