// ==UserScript==
// @name         pr0game Galaxy Control
// @namespace    https://pr0game.pebkac.me/
// @version      0.3
// @description  Control the Galaxy view with your arrow keys.
// @author       AxelFLOSS
// @match        https://pr0game.com/*/game.php?*page=galaxy*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_galaxy_control.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_galaxy_control.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var keypressRegistered = false;
    document.addEventListener('keydown', logKey);

    function logKey(e) {
        if (!keypressRegistered) {
            console.log(e);
            const callback = {
                "a"  : leftHandler,
                "ArrowLeft"  : leftHandler,
                "d"  : rightHandler,
                "ArrowRight" : rightHandler,
                "w"    : upHandler,
                "ArrowUp"    : upHandler,
                "s"  : downHandler,
                "ArrowDown"  : downHandler,
            }[event.key]
            callback?.()
        }
    }

    function leftHandler(){
        location.assign("javascript:galaxy_submit('systemLeft')");
        keypressRegistered = true;
    }
    function rightHandler(){
        location.assign("javascript:galaxy_submit('systemRight')");
        keypressRegistered = true;
    }
    function upHandler(){
        location.assign("javascript:galaxy_submit('galaxyLeft')");
        keypressRegistered = true;
    }
    function downHandler(){
        location.assign("javascript:galaxy_submit('galaxyRight')");
        keypressRegistered = true;
    }
})();
